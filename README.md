# MyAwesomeCV

MyAwesomeCV it's a LaTex template for my CV and Résumé. You can **use and remix** the ```.tex``` templates for personal purposes, but **you're not allowed** to use any of my private documentation/information without my previous authorization.

## How to Use

#### Requirements

A basic **knowhow** in LaTeX2e. You can find plenty of tutorials around the web, IMHO none better than [*The Not To Short Introduction to LaTex2e*](https://ctan.org/tex-archive/info/lshort).

A full TeX **distribution** is assumed. [Various distributions for different operating systems (Windows, Mac, \*nix) are available](http://tex.stackexchange.com/q/55437) but **TeX Live** is recommended.

If you're on Linux open a terminal. Then install TeX Live full with:

```bash
$ sudo apt update
$ sudo apt dist-upgrade
$ sudo apt install texlive-full
```

Please notice that ```texlive-full``` is a meta-package, so it might take a while to download all its dependencies from the internet.

#### Usage

Any TeX editor of your preference will do the trick. My personal choice is [TeXstudio IDE](https://www.texstudio.org/) for editing, compiling, and so on.

In order to install TeXstudio on Linux, open a Terminal and run:
```bash
$ sudo add-apt-repository ppa:sunderme/texstudio
$ sudo apt update
$ sudo apt install texstudio
```

Finally, open the <cv/resume/coverletter>.tex file in the editor and start having some fun editing!

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

## Credit

Fork of posquit0's [Awesome-CV](https://github.com/posquit0/Awesome-CV/) template.
